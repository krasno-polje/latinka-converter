FROM rust:alpine
COPY . /app/
WORKDIR /app
RUN apk add --no-cache musl-dev
RUN cargo build \
    && cp target/debug/latinka-converter /usr/local/bin \
    && cargo clean
ENTRYPOINT ["latinka-converter"]
