use std::{collections::HashMap, fmt, path::Path, fs::File, io::Read};

use regex::{Regex, Captures, Match};
use serde::{Serialize, Deserialize};

fn count_syms_tokens(tokens: &Vec<Token>) -> usize {
    tokens.iter()
        .filter(|t: &&Token| !matches!(t, Token::WordStart) && !matches!(t, Token::WordEnd))
        .count()
}

fn left_tokens_not_match(left_tokens: &Vec<Token>, in_word: &str,
                         letter_index: usize, orthography: &OrthographyMap) -> bool {
    for (t_i, t) in left_tokens.iter().rev().enumerate() {
        if matches!(t, Token::WordStart) {
            let whitespace_or_not_letter_reached: bool = match letter_index.checked_sub(t_i + 1) {
                None => true,
                Some(checked_index) => not_a_letter(in_word.chars()
                    .nth(checked_index).unwrap().to_string().as_str(), &orthography),
            };
            let word_start_not_reached: bool = !whitespace_or_not_letter_reached; //i - t_i != 0;
            return word_start_not_reached;
        }
        let ch_to_check: char = in_word.chars().nth(letter_index - 1 - t_i).unwrap();
        return !char_matches_token(&ch_to_check, t, &orthography);
    }
    return false;
}

fn right_tokens_not_match(right_tokens: &Vec<Token>, in_word: &str, 
                          letter_index: usize, orthography: &OrthographyMap) -> bool {
    for (t_i, t) in right_tokens.iter().enumerate() {
        if matches!(t, Token::WordEnd) {
            let whitespace_or_not_letter_reached: bool = match in_word.chars().nth(letter_index + 1 + t_i) {
                None => true,
                Some(c) => not_a_letter(c.to_string().as_str(), &orthography)
            };
            let word_end_not_reached: bool = !whitespace_or_not_letter_reached;
            return word_end_not_reached;
        }
        let ch_to_check: char = in_word.chars().nth(letter_index + 1 + t_i).unwrap();
        return !char_matches_token(&ch_to_check, t, &orthography);
    }
    return false;
}

fn correct_out_case(rule_out: &str, must_be_uppercase: bool) -> String {
    if must_be_uppercase {
        let rule_out_first_ch: &char = &rule_out.to_uppercase().chars().next().unwrap();
        let rest_of_out: &str = if rule_out.chars().count() > 1 {
            &rule_out[1..]
        } else {
            ""
        };

        [rule_out_first_ch.to_string(), rest_of_out.to_string()].concat()
    } else {
        rule_out.to_owned()
    }
}

fn convert_letter(in_letter: char, in_word: &str, in_letter_index: usize, 
                  is_uppercase: bool, orthography: &OrthographyMap) -> Option<String> {
    let word_left_len: usize = in_letter_index;
    let word_right_len: usize = in_word.chars().count() - in_letter_index - 1;
    for rule in orthography.rules.iter() {
        if rule.unit.letter != in_letter {
            continue;
        }
        if count_syms_tokens(&rule.unit.left) > word_left_len
            || count_syms_tokens(&rule.unit.right) > word_right_len {
            continue;
        }
        if left_tokens_not_match(&rule.unit.left, in_word, in_letter_index, orthography)
            || right_tokens_not_match(&rule.unit.right, in_word, in_letter_index, orthography) {
            continue;
        }
        let out: String = correct_out_case(&rule.out, is_uppercase);
        return Some(out);
    }
    return None;
}

fn convert_word(in_word: &str, orthography: &OrthographyMap) -> String {
    let mut out_word = String::new();
    let in_word_len: usize = in_word.chars().count();
    for in_symbol_index in 0..in_word_len {
        let mut in_symbol: char = in_word.chars().nth(in_symbol_index).expect("Char index out of bound");
        let is_uppercase: bool = in_symbol.is_uppercase();
        if is_uppercase {
            in_symbol = in_symbol.to_lowercase().to_string().chars().nth(0).unwrap();
        }
        match convert_letter(in_symbol, in_word, 
                             in_symbol_index, is_uppercase, orthography) {
            Some(out) => out_word.push_str(out.as_str()),
            None => out_word.push(in_symbol)
        }
    }
    return out_word;
}

pub fn convert_text(text: &str) -> String {
    let orthography: OrthographyMap = load_chars_map();
    text.split_whitespace()
        .map(|word: &str| convert_word(word, &orthography))
        .collect::<Vec<String>>()
        .join(" ")
}

fn convert_text_with_orthography(text: &str, orthography: &OrthographyMap) -> String {
    text.split_whitespace()
        .map(|word: &str| convert_word(word, &orthography))
        .collect::<Vec<String>>()
        .join(" ")
}

fn read_yaml_from_file(orthography_yaml: &mut String, orthography_file_path: &str) {
    let path = Path::new(orthography_file_path);
    let mut file = File::open(path).expect("File with such a path does not exist");
    file.read_to_string(orthography_yaml).expect("Cannot read file content");
}

pub fn convert_text_with_orthography_path(text: &str, orthography_file_path: &str) -> String {
    let mut orthography_yaml = String::new();
    read_yaml_from_file(&mut orthography_yaml, orthography_file_path);
    let orthography_raw: OrthographyRawMap = serde_yaml::from_str(orthography_yaml.as_str())
        .expect("Cannot parse yaml orthography definition");
    let orthography: OrthographyMap = conv_raw_map(&orthography_raw);
    return convert_text_with_orthography(text, &orthography);
}

fn load_chars_map() -> OrthographyMap {
    let raw_orthography: OrthographyRawMap = load_chars_raw_map();
    return conv_raw_map(&raw_orthography);
}

fn not_a_letter(sym: &str, orthography: &OrthographyMap) -> bool {
    let letters: Vec<String> = [
        orthography.vowels.as_slice(), 
        orthography.consonants.as_slice(), 
        orthography.others.as_slice()
    ].concat();
    return !letters.contains(&sym.to_lowercase().to_string());        
}

fn parse_tokens(orth: &str) -> Vec<Token> {
    let mut tokens: Vec<Token> = Vec::new();
    let mut in_brackets: bool = false;
    let mut group_buf: Vec<char> = Vec::new();
    for ch in orth.chars() {
        if in_brackets {
            match &ch {
                ')' => {
                    let token: Token = Token::Group(group_buf.clone());
                    tokens.push(token);
                    group_buf.clear();
                    in_brackets = false;
                },
                '|' => {},
                _default => group_buf.push(ch)
            }
            continue;
        }
        match &ch {
            '(' => in_brackets = true,
            'V' => tokens.push(Token::Vowel),
            'C' => tokens.push(Token::Consonant),
            '^' => tokens.push(Token::WordStart),
            '$' => tokens.push(Token::WordEnd),
            _default => {}
        }
    }
    return tokens;
}

#[derive(Serialize, Deserialize)]
struct OrthographyRawMap {
    rules: Vec<(String, String)>,
    default_rules: HashMap<String, String>,
    vowels: Vec<String>,
    consonants: Vec<String>,
    others: Vec<String>,
}

impl OrthographyRawMap {
    fn get_all_syms(&self) -> Vec<String> {
        [self.vowels.as_slice(), 
         self.consonants.as_slice(), 
         self.others.as_slice()]
        .concat()
    }
}

#[derive(Serialize, Deserialize)]
struct OrthographyMap {
    rules: Vec<Rule>,
    default_rules: HashMap<String, String>,
    vowels: Vec<String>,
    consonants: Vec<String>,
    others: Vec<String>,
}

#[derive(Serialize, Deserialize)]
struct OrthographyUnit {
    left: Vec<Token>,
    letter: char,
    right: Vec<Token>
}

#[derive(Serialize, Deserialize)]
struct Rule {
    unit: OrthographyUnit,
    out: String
}

fn split_orth_unit_str(rule: &str, raw_orthography: &OrthographyRawMap) -> (String, String, String) {
    let supported_letters: Vec<String> = raw_orthography.get_all_syms();
    let regex_str = format!(
        r"((\(([{letters}]\|?)+\)|[CV^$])*)([{letters}])((\(([{letters}]\|?)+\)|[CV^$])*)", 
        letters = supported_letters.concat());
    let main_regex = Regex::new(&regex_str).unwrap();
    let captures: Captures = main_regex.captures(rule).unwrap();
    let left: &str = captures.get(1).map_or("", |m: Match| m.as_str());
    let right: &str = captures.get(5).map_or("", |m: Match| m.as_str());
    let letter: &str = captures.get(4).expect("Center orthography element is required").as_str();
    return (String::from(left), String::from(letter), String::from(right));
}

fn conv_rules(rules: &Vec<(String, String)>, raw_orthography: &OrthographyRawMap) -> Vec<Rule> {
    rules.iter()
        .map(|orth_unit_to_syms: &(String, String)| {
            let (left, letter, right) = split_orth_unit_str(orth_unit_to_syms.0.as_str(), raw_orthography);
            let orth_unit = OrthographyUnit {
                left: parse_tokens(&left), 
                letter: letter.chars().nth(0).unwrap(), 
                right: parse_tokens(&right)
            };
            Rule {
                unit: orth_unit,
                out: orth_unit_to_syms.1.clone()
            }
        })
        .collect()
}

fn conv_raw_map(raw_map: &OrthographyRawMap) -> OrthographyMap {
    OrthographyMap { 
        rules: conv_rules(&raw_map.rules, raw_map), 
        default_rules: raw_map.default_rules.clone(), 
        vowels: raw_map.vowels.clone(),
        consonants: raw_map.consonants.clone(), 
        others: raw_map.others.clone()
    }
}

#[derive(Serialize, Deserialize)]
enum Token {
    Group(Vec<char>),
    WordStart,
    WordEnd,
    Vowel,
    Consonant,
}

fn char_matches_token(ch: &char, token: &Token, orthography: &OrthographyMap) -> bool {
    match token {
        Token::Group(criterions_chars) 
            => criterions_chars.iter().any(|criterion: &char| 
                ch.to_lowercase().to_string() == criterion.to_string()),
        Token::Vowel => orthography.vowels.iter().any(|criterion: &String| 
            criterion.eq(&ch.to_lowercase().to_string())),
        Token::Consonant => orthography.consonants.iter().any(|criterion: &String| 
            criterion.eq(&ch.to_lowercase().to_string())),
        _default => false
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            Token::Group(chs) => {
                let chs_str_vec: Vec<String> = chs.iter()
                    .map(|c: &char| c.to_string())
                    .collect();
                write!(f, "[Group: {{{}}}]", chs_str_vec.join(", "))
            },
            Token::Consonant => write!(f, "[CONSONANT]"),
            Token::Vowel     => write!(f, "[VOWEL]"),
            Token::WordStart => write!(f, "[START]"),
            Token::WordEnd   => write!(f, "[END]"),
        }
    }
}

// fn tokens_vec_to_string(tokens: &Vec<Token>) -> String {
//     let mut result: String = String::new();
//     result.push('(');
//     let tokens_as_strs: Vec<String> = tokens.iter()
//         .map(|t: &Token| t.to_string())
//         .collect();
//     let tokens_str: String = tokens_as_strs.join(", ");
//     result.push_str(&tokens_str);
//     result.push(')');
//     return result;
// }

fn load_chars_raw_map() -> OrthographyRawMap {
    OrthographyRawMap { 
        rules: Vec::from([
            ("а".to_owned(), "a".to_owned()), // масло -> maslo
            ("^я".to_owned(), "ja".to_owned()), // яйцо -> jaićo
            ("(ы)я".to_owned(), "ia".to_owned()),
            ("Vя".to_owned(), "ja".to_owned()), // моя -> moja
            ("Cя".to_owned(), "ä".to_owned()), // мясо -> mäso 
            ("(ь|ъ)я".to_owned(), "a".to_owned()), // полынья -> polynnja

            ("э".to_owned(), "e".to_owned()), // рэп, это -> rep, eto

            ("^е".to_owned(), "je".to_owned()), // еда -> jeda
            ("(и)е$".to_owned(), "e".to_owned()), // причастие -> pricastie
            ("(ы)е".to_owned(), "ie".to_owned()),
            ("Vе".to_owned(), "je".to_owned()), // поезд -> pojezd
            ("Cе".to_owned(), "e".to_owned()), // лето -> leto
            ("(ь|ъ)е".to_owned(), "e".to_owned()), // съел печенье -> sjel pecennje

            ("^ё".to_owned(), "jė".to_owned()), // ёж -> jėž
            ("(ы)ё".to_owned(), "iė".to_owned()),
            ("Vё".to_owned(), "jė".to_owned()), // моё -> mojė
            ("Cё".to_owned(), "ė".to_owned()), // тёмный -> tėmnyi
            ("(ь|ъ)ё".to_owned(), "ė".to_owned()), // питьё -> pittjė

            ("^и".to_owned(), "i".to_owned()),
            ("Vи".to_owned(), "ji".to_owned()),
            ("(ш|ж|ч|ц|щ|к|г)и(й)".to_owned(), "y".to_owned()), // related to other rule
            ("Cи".to_owned(), "i".to_owned()),
            ("(ь|ъ)и".to_owned(), "i".to_owned()),

            ("о".to_owned(), "o".to_owned()),

            ("у".to_owned(), "u".to_owned()),

            ("ы".to_owned(), "y".to_owned()),

            ("^ю".to_owned(), "ju".to_owned()), // юг -> jug
            ("(ы)ю".to_owned(), "iu".to_owned()),
            ("Vю".to_owned(), "ju".to_owned()), // знаю -> znaju
            ("Cю".to_owned(), "ü".to_owned()), // блюдо -> blüdo
            ("(ь|ъ)ю".to_owned(), "u".to_owned()), // муравью -> murawju

            ("б(ь)".to_owned(), "bb".to_owned()),
            ("б".to_owned(), "b".to_owned()),

            ("ч".to_owned(), "c".to_owned()),
            ("ц".to_owned(), "ć".to_owned()),

            ("д(ь)".to_owned(), "dd".to_owned()),
            ("д".to_owned(), "d".to_owned()),

            ("ф(ь)".to_owned(), "ff".to_owned()),
            ("ф".to_owned(), "f".to_owned()),

            ("г(ь)".to_owned(), "gg".to_owned()),
            ("г".to_owned(), "g".to_owned()),

            ("х(ь)".to_owned(), "hh".to_owned()),
            ("х".to_owned(), "h".to_owned()),

            ("к(ь)".to_owned(), "kk".to_owned()),
            ("к".to_owned(), "k".to_owned()),

            ("л(ь)".to_owned(), "ll".to_owned()),
            ("л".to_owned(), "l".to_owned()),

            ("м(ь)".to_owned(), "mm".to_owned()),
            ("м".to_owned(), "m".to_owned()),

            ("н(ь)".to_owned(), "nn".to_owned()),
            ("н".to_owned(), "n".to_owned()),

            ("п(ь)".to_owned(), "pp".to_owned()),
            ("п".to_owned(), "p".to_owned()),

            ("р(ь)".to_owned(), "rr".to_owned()),
            ("р".to_owned(), "r".to_owned()),

            ("с(ь)$".to_owned(), "ss".to_owned()),
            ("с(ь)".to_owned(), "ś".to_owned()),
            ("с".to_owned(), "s".to_owned()),

            ("ш(ь)V".to_owned(), "šš".to_owned()),
            ("ш(ь)".to_owned(), "š".to_owned()),
            ("ш".to_owned(), "š".to_owned()),
        
            ("щ(ь)V".to_owned(), "šcc".to_owned()),
            ("щ(ь)".to_owned(), "šc".to_owned()),
            ("щ".to_owned(), "šc".to_owned()),

            ("т(ь)".to_owned(), "tt".to_owned()),
            ("т".to_owned(), "t".to_owned()),

            ("в(ь)".to_owned(), "w".to_owned()), // морковь -> morkow
            ("в".to_owned(), "v".to_owned()),

            ("з(ь)".to_owned(), "zz".to_owned()), // мазь -> mazz
            ("з".to_owned(), "z".to_owned()),

            ("ж(ь)V".to_owned(), "žž".to_owned()), // божья -> božžja
            ("ж(ь)".to_owned(), "ž".to_owned()), // божья -> božžja
            ("ж".to_owned(), "ž".to_owned()),

            ("(ы)й".to_owned(), "i".to_owned()),
            ("^й".to_owned(), "j".to_owned()),
            ("(и)йC".to_owned(), "j".to_owned()),
            ("VйC".to_owned(), "i".to_owned()),
            ("(ш|ж|ч|ц|щ|к|г)(и)й".to_owned(), "i".to_owned()),
            ("(и)й$".to_owned(), "j".to_owned()),
            ("Vй$".to_owned(), "i".to_owned()),
            ("й".to_owned(), "j".to_owned()),

            ("Cь$".to_owned(), "".to_owned()),
            ("CьC".to_owned(), "".to_owned()),
            ("Cь(е|ё|ю|я|и)".to_owned(), "j".to_owned()),

            ("ъ$".to_owned(), "".to_owned()),
            ("Cъ(е|ё|ю|я|и)".to_owned(), "j".to_owned()),
        ]),
        default_rules: HashMap::from([
            ("а".to_owned(), "a".to_owned()),
            ("б".to_owned(), "b".to_owned()),
            ("в".to_owned(), "v".to_owned()),
            ("г".to_owned(), "g".to_owned()),
            ("д".to_owned(), "d".to_owned()),
            ("е".to_owned(), "je".to_owned()),
            ("ё".to_owned(), "jė".to_owned()),
            ("ж".to_owned(), "ž".to_owned()),
            ("з".to_owned(), "z".to_owned()),
            ("и".to_owned(), "i".to_owned()),
            ("й".to_owned(), "j".to_owned()),
            ("к".to_owned(), "k".to_owned()),
            ("л".to_owned(), "l".to_owned()),
            ("м".to_owned(), "m".to_owned()),
            ("н".to_owned(), "n".to_owned()),
            ("о".to_owned(), "o".to_owned()),
            ("п".to_owned(), "p".to_owned()),
            ("р".to_owned(), "r".to_owned()),
            ("с".to_owned(), "s".to_owned()),
            ("т".to_owned(), "t".to_owned()),
            ("у".to_owned(), "u".to_owned()),
            ("ф".to_owned(), "f".to_owned()),
            ("х".to_owned(), "h".to_owned()),
            ("ц".to_owned(), "ć".to_owned()),
            ("ч".to_owned(), "c".to_owned()),
            ("ш".to_owned(), "š".to_owned()),
            ("щ".to_owned(), "šc".to_owned()),
            ("ъ".to_owned(), "".to_owned()),
            ("ы".to_owned(), "y".to_owned()),
            ("ь".to_owned(), "".to_owned()),
            ("э".to_owned(), "e".to_owned()),
            ("ю".to_owned(), "ju".to_owned()),
            ("я".to_owned(), "ja".to_owned()),
        ]),
        vowels: vec![
            "а".to_owned(), 
            "е".to_owned(), 
            "ё".to_owned(), 
            "и".to_owned(), 
            "о".to_owned(), 
            "у".to_owned(), 
            "ы".to_owned(), 
            "э".to_owned(), 
            "ю".to_owned(), 
            "я".to_owned(),
        ], 
        consonants: vec![
            "б".to_owned(), 
            "в".to_owned(), 
            "г".to_owned(), 
            "д".to_owned(), 
            "ж".to_owned(), 
            "з".to_owned(), 
            "й".to_owned(), 
            "к".to_owned(), 
            "л".to_owned(), 
            "м".to_owned(), 
            "н".to_owned(), 
            "п".to_owned(), 
            "р".to_owned(), 
            "с".to_owned(), 
            "т".to_owned(), 
            "ф".to_owned(), 
            "х".to_owned(), 
            "ц".to_owned(), 
            "ч".to_owned(), 
            "ш".to_owned(), 
            "щ".to_owned()
        ],
        others: vec!["ъ".to_owned(), "ь".to_owned()]
    }
}