use std::{path::Path, fs::File, io::{Read, self}};

use clap::{Parser, Subcommand};

use crate::latinka_converter;

fn get_input_text(input_as_arg: &Option<String>, input_file_path: &Option<String>) -> String {
    if input_as_arg.is_some() {
        return input_as_arg.as_ref().map_or(String::new(), |s| s.clone());
    }
    let path_str: String = input_file_path.as_ref()
        .map_or(String::new(), |s| s.clone());
    let mut input_text = String::new();
    if path_str == "-" {
        io::stdin().read_to_string(&mut input_text)
            .expect("Cannot read from input");
        return input_text;
    }
    let path: &Path = Path::new(path_str.as_str());
    let mut file: File = File::open(path)
        .expect("File with such a name does not exist");
    file.read_to_string(&mut input_text)
        .expect("Cannot read from file");
    return input_text;
}

pub fn run() {
    let cli = Cli::parse();
    match &cli.command {
        Command::Conv { input: input_as_arg, input_file: input_file_path, 
                        orthography_file_path } => {
            if input_as_arg.as_ref().is_none() && input_file_path.as_ref().is_none() {
                println!("Please, provide a text to convert.");
                return;
            }
            let input_text: String = get_input_text(input_as_arg, input_file_path);
            let output: String = match orthography_file_path {
                Some(file_path) => latinka_converter::convert_text_with_orthography_path(
                    input_text.as_str(), file_path),
                None => latinka_converter::convert_text(input_text.as_str()),
            };
            println!("{}", output);
        },
    }
}

#[derive(Parser)]
#[clap(name = "latinka-converter")]
#[clap(author = "gastipatis <gastipatis@gmail.com>")]
#[clap(version = "1.0")]
#[clap(about = "Tool for text convertion from one orthography to another")]
struct Cli {
    #[clap(subcommand)]
    command: Command
}

#[derive(Subcommand)]
enum Command {
    /// Convert text from one orthography to another according to a specification
    Conv {
        #[clap(short, long, action, group = "first")]
        input: Option<String>,
        #[clap(short='f', long = "file", action, group = "first")]
        input_file: Option<String>,
        #[clap(short, long = "orth-file-path", action)]
        orthography_file_path: Option<String>
    },
}