extern crate serde;

mod cli;
mod latinka_converter;
#[cfg(test)]
mod tests;

fn main() {
    cli::run();
}