# In host environment

## Requirements

[cargo package manager](https://github.com/rust-lang/cargo)
or
[docker](https://www.docker.com)

## Compile

```bash 
cargo build
```

## Run

```bash 
./target/debug/latinka-converter conv -i "Здравствуй, мир" -o orthographies/latinka.yml
```

# In Docker

## Create image from dockerfile

```bash 
docker build . -t latinka-converter
```

## Run container with the image

```bash 
docker run -it --rm latinka-converter conv -i "Здравствуй, мир" -o orthographies/latinka.yml
```
